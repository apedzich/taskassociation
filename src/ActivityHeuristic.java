/*
 * Klasa odpowiadaj�ca heurystyce aktywno�ci dotycz�cej b��d�w
 */
public class ActivityHeuristic {
	
	/*
	 * Status zamkni�tych b��d�w
	 */
	private final String statusForClosed;
	/*
	 * Resolution zamkni�tych b��d�w
	 */
	private final String resolutionForClosed;
	/*
	 * Heurystyka dla solvera
	 */
	private final Pair<String, String> solverHeuristic;
	/*
	 * Heurystyka dla reviewera
	 */
	private final Pair<String, String> reviewerHeuristic;
	
	public ActivityHeuristic(String statusForClosed, String resolutionForClosed, 
			Pair<String, String> solverHeuristic, Pair<String, String> reviewerHeuristic) {
		this.statusForClosed = statusForClosed;
		this.resolutionForClosed = resolutionForClosed;
		this.solverHeuristic = solverHeuristic;
		this.reviewerHeuristic = reviewerHeuristic;
	}
	
	String getStatusForClosed() {
		return statusForClosed;
	}
	
	String getResolutionForClosed() {
		return resolutionForClosed;
	}
	
	Pair<String, String> getSolverHeuristic() {
		return solverHeuristic;
	}
	
	Pair<String, String> getReviewerHeuristic() {
		return reviewerHeuristic;
	}

}
