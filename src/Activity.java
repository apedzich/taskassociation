/*
 * Klasa odpowiadaj�ca jednemu wpisowi w historii aktywno�ci dotycz�cej b��du
 */
public class Activity {
	
	/*
	 * Login u�ytkownika 
	 */
	private final String who;
	/*
	 * Nazwa zmienionego pola (nazwy takie jak w repozytorium Bugzilli)
	 */
	private final String what;
	/*
	 * Stara warto�� zmienionego pola
	 */
	private final String removed;
	/*
	 * Nowa warto�� zmienionego pola
	 */
	private final String added;
	
	public Activity(String who, String what, String removed, String added) {
		this.who = who;
		this.what = what;
		this.removed = removed;
		this.added = added;
	}
	
	String getWho() {
		return who;
	}
	
	String getWhat() {
		return what;
	}
	
	String getRemoved() {
		return removed;
	}
	
	String getAdded() {
		return added;
	}
	
}
