/*
 * Klasa odpowiadaj�ca u�ytkownikowi repozytorium
 */
public class User implements Comparable<User> {
	
	/*
	 * Login u�ytkownika
	 */
	private final String login;

    
    public User(String login) {  
    	this.login = login;
    }
	
    public String getLogin() {
    	return login;
    }
    
    /*
     * Funkcja sprawdzaj�ca czy u�ytkownik jest aktywny
     */
    public Boolean isActive() {
    	return (!login.contains("(gone)") && !login.contains("(inactive)") && !login.contains("(fired)"));
    }
    
    @Override
    public String toString() {
    	return login;
    }
    
    @Override 
    public int compareTo(User user) {
    	return this.login.compareTo(user.getLogin());
    }
	 
}
