import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class TaskAssociation {

	public static void main(String[] args) {
	    try {	
	    	if (args.length < 14) {
	    		System.err.println("Not enough parameters.");
	    		return;
			}
	
			String projectName = args[0];
			String url = args[1];
	
			BugRepository r = new BugRepository(url);
		
			String excludeWordsFilename = args[2];
		
			ArrayList<String> excludeWords = new ArrayList<String>();
			if (excludeWordsFilename.equals("null"))
				excludeWords = null;
			else {
				File file = new File(excludeWordsFilename);
		    	Scanner in = new Scanner(file);
		    	String str;
		    	while(in.hasNextLine()){
		    		str = in.nextLine();
		    		excludeWords.add(str);
		    	}
		    	in.close();
			}
			
			String activityHeuristicFilename = args[3];
			
			File file = new File(activityHeuristicFilename);
	    	Scanner in = new Scanner(file);
	    	String status = in.nextLine();
	    	String resolution = in.nextLine();
	    	String what1 = in.nextLine();
	    	String added1 = in.nextLine();
	    	String what2 = in.nextLine();
	    	String added2 = in.nextLine();
	    	in.close();
	    	
	    	ActivityHeuristic activityHeuristic = new ActivityHeuristic(status, resolution, new Pair<String, String>(what1, added1), new Pair<String, String>(what2, added2));
			
	    	int minLimit = Integer.parseInt(args[4]);
	    	
	    	boolean includeAssigners = Boolean.parseBoolean(args[5]);
	    	boolean includeSolvers = Boolean.parseBoolean(args[6]);
	    	boolean includeReviewers = Boolean.parseBoolean(args[7]);
	    	
	    	String dateFrom = args[8];
	    	String orderBy = args[9];
	    	String stopWordsFilename = args[13];
	    	
	    	System.out.println("Generating list of bugs");
			ArrayList<Bug> solvedBugsList = r.getSolvedBugs(MethodType.HTML, projectName + "Bugs.txt", projectName + "Time.txt", projectName + "Desc.txt", projectName + "Errors.txt", stopWordsFilename, minLimit, includeAssigners, includeSolvers, includeReviewers, activityHeuristic, excludeWords, dateFrom, orderBy);
			//ArrayList<Bug> solvedBugsList = r.getSolvedBugsFromFile("CompendiumBugs.txt", minLimit, projectName + "Bugs.txt", projectName + "Desc.txt", projectName + "Time.txt", projectName + "Errors.txt");
	    	
			int testSetNum = Integer.parseInt(args[10]);
			ArrayList<Integer> successesList = new ArrayList<Integer>();
			int allSuccesses = 0;
			for (int k = 0; k <= 9; k++) {
				ArrayList<Bug> tmpSolvedBugsList = new ArrayList<Bug>(solvedBugsList);
				
				Random randomGenerator = new Random();
				ArrayList<Bug> testBugsList = new ArrayList<Bug>();
				for (int i = 0; i < testSetNum; i++) {
					int randomId = 0;
					for (int j = 0; j < 200; j++) {
						randomId = randomGenerator.nextInt(tmpSolvedBugsList.size());
						System.out.println(randomId);
						if (tmpSolvedBugsList.get(randomId).getSolver() != null)
							break;
					}
					testBugsList.add(tmpSolvedBugsList.get(randomId));
					tmpSolvedBugsList.remove(randomId);
				}
				
				int topicsNum = Integer.parseInt(args[11]);
				
				System.out.println("Generating topic model");
				ArrayList<Topic> topicsList = r.getTopicsFromTopicsModel(topicsNum, projectName + "Topics.txt", projectName + "Desc.txt", projectName + "Time.txt", stopWordsFilename);
				//ArrayList<Topic> topicsList = r.getTopicsFromFile(projectName + "Topics.txt");
				
				System.out.println("Generating list of users");
				ArrayList<User> usersList = r.getUsers(tmpSolvedBugsList, projectName + "Users.txt", projectName + "Time.txt");
				
				System.out.println("Generating actions counters");
				ArrayList<Integer> actionsCounters = r.getActionsCounters(tmpSolvedBugsList, includeAssigners, includeSolvers, includeReviewers, projectName + "Counters.txt", projectName + "Time.txt");
				
				System.out.println("Generating bugs topics matrix");
				BugsTopicsMatrix btm = r.generateBugsTopicsMatrix(tmpSolvedBugsList, topicsList, null, projectName + "Time.txt");
				
				System.out.println("Generating topics users matrix");
				TopicsUsersMatrix tum = r.generateTopicsUsersMatrix(btm, usersList, tmpSolvedBugsList, includeAssigners, includeSolvers, includeReviewers, null, projectName + "Time.txt");
				
				System.out.println("Generating score matrix");
				ScoreMatrix score = r.generateTopicsUsersMatrixScore(tmpSolvedBugsList, topicsList, usersList, actionsCounters.get(0), actionsCounters.get(1), actionsCounters.get(2), null, projectName + "Time.txt");
			
				int successes = 0;
				
				int sigNum = Integer.parseInt(args[11]);
				int listSize = Integer.parseInt(args[12]);
				
				for (int i = 0; i < testBugsList.size(); i++) {
					ArrayList<User> usersForBug = r.generateUsersListForBug(testBugsList.get(i).getId(), MethodType.HTML, stopWordsFilename, activityHeuristic, excludeWords, 
							null, projectName + "_" + testBugsList.get(i).getId() + ".txt",
							btm, tum, score, usersList, topicsList, includeAssigners, includeSolvers, includeReviewers, sigNum, listSize);
					
					System.out.println(k + " " + i);
					for (int j = 0; j < listSize; j++) {
						if (testBugsList.get(i).getSolver() != null) {
							if (usersForBug.get(j).getLogin().equals(testBugsList.get(i).getSolver().getLogin())) {
								successes++;
								break;
							}
						}	
						else if ((testBugsList.get(i).getAssigner() != null) || (testBugsList.get(i).getReviewer() != null)) {
							if (testBugsList.get(i).getAssigner() != null && usersForBug.get(j).getLogin().equals(testBugsList.get(i).getAssigner().getLogin())) {
								successes++;
								break;
							}
							else if (testBugsList.get(i).getReviewer() != null && usersForBug.get(j).getLogin().equals(testBugsList.get(i).getReviewer().getLogin())) {
								successes++;
								break;
							}
						}
					}
					
				}
			
				//System.out.println(successes + " successes out of " + testBugsList.size());
				successesList.add(successes);
				allSuccesses += successes;
				
			}
			for (int i = 0; i < 10; i++) {
				System.out.println(successesList.get(i) + " successes out of " + testSetNum);
			}
			double allSuccessesDouble = (double) allSuccesses;
			double successesMean = allSuccessesDouble/10.0;
			System.out.println("Mean of successes in 10 iterations " + successesMean);
			
	    }
	    catch (FileNotFoundException e) {
	    	System.err.println("FileNotFoundException occured.");
	    }
	}
	
}
