import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.InstanceList;


/*
 * Klasa odpowiadaj�ca repozytorium Bugzilli
 */
public class BugRepository {
	
	/*
	 * Adres repozytorium
	 */
	private final String url;
	
	public BugRepository(String url) {
		this.url = url;
	}

	String getUrl() {
		return url;
	}
	
	/*
	 * Funkcja pomocnicza
	 * fileName - nazwa pliku
	 */
	private String[] loadListFromFile(String fileName) throws IOException {
		File fileReader = new File(fileName);
		Scanner inputFile = new Scanner(fileReader);
		List<String> list = new ArrayList<String>();
		while (inputFile.hasNextLine()) {
			list.add(inputFile.nextLine());
		}
		inputFile.close();
		return list.toArray(new String[list.size()]);
	}
	
	/*
	 * Funkcja pozwalaj�ca na czytanie zawartosci strony bez sprawdzania certyfikat�w
	 */
	private void allowWithoutCheckingCerts() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}
		} };
		try {
			SSLContext sc;
			sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithException happened while working with certificates.\n");
			e.printStackTrace();
		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException happened while working with certificates.\n");
			e.printStackTrace();
		}
	}
	
	/*
	 * Generowanie JSON-a ze strony o b��dzie
	 * url - adres strony 
	 * bugId - id b��du 
	 * fwErrors - FileWriter do zapisywania komunikat�w o b��dach 
	 */
	private JSONObject generateJSON (String url, int bugId, FileWriter fwErrors) {	
		JSONObject obj = null;	
		try {		
			URL realUrl = new URL(url);
        	BufferedReader in = new BufferedReader(new InputStreamReader(realUrl.openStream()));
        	String json = "";
        	String inputLine;
        	int cond = 0;     	
        	while ((inputLine = in.readLine()) != null) {
        		inputLine = StringEscapeUtils.unescapeHtml3(inputLine);
        		if ((inputLine.contains("<pre>")) && (cond == 0)) {
        			json += "{";
        			cond = 1;
        		}
        		else if (cond == 1) {
        			inputLine = new String(inputLine.getBytes(), Charset.forName("UTF-8"));
        			json += inputLine;
        		} 
        		else if (inputLine.contains("</pre>")) {
        			break;
        		}   
        	}    	
        	in.close(); 
        	obj = new JSONObject(json);   
		}
		catch (MalformedURLException e) {
			System.err.println("MalformedException happened while generating JSON about bug " + bugId + ".\n");
			e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("In generateJSON MalformedException happened while generating JSON about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
		}
        catch (IOException e) {
        	System.err.println("IOException happened while generating JSON about bug " + bugId + ".\n");
        	e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("In generateJSON IOException happened while generating JSON about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
      
        }
		catch (JSONException e) {
			System.err.println("JSONException happened while generating JSON about bug " + bugId + ".\n");
			e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("JSONException IOException happened while generating JSON about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
		}	
		return obj;		
	}
	
	/*
	 * Funkcja zwracaj�ca konkretne pole z g��wnej strony raportu o b��dzie
	 * bugId - id b��du 
	 * fieldName - nazwa pola, kt�re chcemy otrzyma� 
	 * err - ko�c�wka komunikatu na wypadek wyst�pienia wyj�tku
	 * fwErrors - FileWriter do zapisywania komunikat�w o b��dach 
	 */
	private String getFieldValue(int bugId, String fieldName, String err, FileWriter fwErrors) {
		String fieldValue = "";
		try {
			// Czytanie strony bez sprawdzania komunikat�w
			allowWithoutCheckingCerts();
			String descUrl = url + "/show_bug.cgi?id=" + bugId;
			URL realUrl = new URL(descUrl);
			
			// Sczytywanie zawarto�ci strony na zmienn�
			BufferedReader in = new BufferedReader(new InputStreamReader(realUrl.openStream()));
			String inputLine;
			String result = "";
			while ((inputLine = in.readLine()) != null)
				result = result + inputLine + "\n";
			in.close();

			Document doc = Jsoup.parse(result); // Sparsowanie html
			// Wyszukanie pierwszego tagu pasuj�cego do patternu
			if (doc == null) 
				return fieldValue;
			Element elem = doc.select(fieldName).first();
			if (elem != null) {
				// Usuni�cie tag�w html
				fieldValue = Jsoup.parse(elem.toString()).text(); 
			}				
		} 
		catch (MalformedURLException e) {
			System.err.println("MalformedURLException happened" + err);
			e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("In getFieldValue MalformedURLException happened while getting data about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened" + err);
			e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("In getFieldValue IOException happened while getting data about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
		}
		return fieldValue;
	}
	
	/*
	 * Funkcja zwracaj�ca nazw�/tytu� b��du
	 * bugId - id b��du
	 * methodType - type metody
	 * fwErrors - FileWriter do zapisywania komunikat�w o b��dach 
	 */
	public String getBugTitle(int bugId, MethodType methodType, FileWriter fwErrors) {
		String title = getFieldValue(bugId, "title", " while getting title of bug " + bugId + ".\n", fwErrors);
		if (title.equals("Invalid Bug ID")) 
			return "";
		return title.replaceFirst("Bug", "");
	}
	
	/*
	 * Funkcja zwracaj�ca komponent b��du
	 * bugId - id b��du
	 * methodType - typ metody
	 * fwErrors - FileWriter do zapisywania komunikat�w o b��dach
	 */
	public String getBugComponent(int bugId, MethodType methodType, FileWriter fwErrors) {
		String component;
		if (methodType.equals(MethodType.REST)) {
			JSONObject obj = generateJSON(url + "rest/bug/" + bugId + "?include_fields?include_fields=component", bugId, fwErrors);   
			component = obj.getJSONArray("bugs").getJSONObject(0).getString("component");
		}
		else {
			component = getFieldValue(bugId, "#field_container_component", " while getting description of bug " + bugId + ".\n", fwErrors);
		}
		return component;
	}
	
	/*
	 * Funkcja zwracaj�ca opis b��du
	 * bugId - id b��du
	 * methodType - typ metody
	 * fwErrors - FileWriter do zapisywania komunikat�w o b��dach
	 */
	public String getBugDescription(int bugId, MethodType methodType, FileWriter fwErrors) {
		String desc;
		if (methodType.equals(MethodType.REST)) {
			JSONObject obj = generateJSON(url + "rest/bug/" + bugId + "/comment?include_fields=author,text", bugId, fwErrors);  
			JSONArray arr = obj.getJSONObject("bugs").getJSONObject(Integer.toString(bugId)).getJSONArray("comments");
			desc = arr.getJSONObject(0).getString("text");
		}
		else {
			desc = getFieldValue(bugId, ".bz_comment_text", " while getting description of bug " + bugId + ".\n", fwErrors);
			if (desc.equals("")) 
				desc = getFieldValue(bugId, "#comment_text_0", " while getting description of bug " + bugId + ".\n", fwErrors);
		}
		
		Scanner scanner = new Scanner(desc);
		String description = "";
		while (scanner.hasNextLine()) {
		  String line = scanner.nextLine();	
		  if (line.contains("------- Additional Comments From"))
			  break;
		  if (line.contains("------- Bug moved to this database by"))
			  break;
		  description += line.toLowerCase() + "\n";	  
		}
		scanner.close();
		return description;
	}
	
	/*
	 * Funkcja zwracaj�ca aktywno�� dotycz�c� b��du
	 * bugId - id b��du
	 * methodType - typ metody
	 * fwErrors - FileWriter do zapisywania komunikat�w o b��dach
	 */
	public ArrayList<Activity> getBugActivity(int bugId, MethodType methodType, FileWriter fwErrors) {
		ArrayList<Activity> activitiesList = new ArrayList<Activity>();
		String tmp = "", who = "", what = "", removed = "", added = "";
		
		try {
			if (methodType.equals(MethodType.REST)) {
				JSONObject obj = generateJSON(url + "rest/bug/" + bugId + "/history?include_fields=field_name,added,removed,who", bugId, fwErrors); 
				JSONArray arr = obj.getJSONArray("bugs").getJSONObject(0).getJSONArray("history");
	    		JSONArray changes;
	    		for (int i = 0; i < arr.length(); i++) {
	    			changes = arr.getJSONObject(i).getJSONArray("changes");
	    			for (int j = 0; j < changes.length(); j++) {
	    				what = changes.getJSONObject(j).getString("field_name");
	    				who = changes.getJSONObject(j).getString("who");
	    				added = changes.getJSONObject(j).getString("added");
	    				removed = changes.getJSONObject(j).getString("removed");
	    				activitiesList.add(new Activity(who, what, removed, added));
	    			}
	    		}	
			}
			else {
				allowWithoutCheckingCerts();
				String descUrl = url + "/show_activity.cgi?id=" + bugId;
				URL realUrl = new URL(descUrl);

				BufferedReader in = new BufferedReader(new InputStreamReader(realUrl.openStream()));
				String inputLine;
				String result = "";

				while ((inputLine = in.readLine()) != null)
					result = result + inputLine + "\n";
				in.close();
				if (result.contains("No changes have been made to this bug yet."))
					return activitiesList;

				Document doc = Jsoup.parse(result); // Sparsowanie html
				if (doc == null) 
					return activitiesList;
				// Wyszukanie warto�ci w polach tablicy
				Elements elems = doc.select("td"); 
				// By� mo�e strona z aktywno�ci b��du jest pusta
				if (elems == null || elems.size() < 5) 
					return activitiesList;

				if (elems.get(0).attr("id").equals("title"))
					elems.remove(0);
				if (elems.get(0).text().equals(""))
					elems.remove(0);
				int i = 0, j = 0, z = 0;
				int rowSpanWho = 0;
				int rowSpanWhen = 0;
				// Czytanie z tablicy
				for (Element elem : elems) {
					if (i == 0 && rowSpanWho > 1) {
						i = (i + 1) % 5;
						rowSpanWho--;
					}
					if (i == 1 && rowSpanWhen > 1) {
						i = (i + 1) % 5;
						rowSpanWhen--;
					}
					if (i == 0) { // U�ytkownik
						tmp = elem.toString();
						j = tmp.indexOf("rowspan=");
						z = tmp.indexOf("\"", j + 9);
						rowSpanWho = Integer.parseInt(tmp.substring(j + 9, z));
						who = elem.text();
					} 
					else if (i == 1) { // Czas
						tmp = elem.toString();
						j = tmp.indexOf("rowspan=");
						z = tmp.indexOf("\"", j + 9);
						rowSpanWhen = Integer.parseInt(tmp.substring(j + 9, z));
					} 
					else if (i == 2) { // Nazwa pola
						what = elem.text();
					} 
					else if (i == 3) { // Stara warto�� pola
						removed = elem.text();
					} 
					else { // Nowa warto�� pola
						added = elem.text();
						activitiesList.add(new Activity(who, what, removed, added));
					}
					i = (i + 1) % 5;
				}
			}
		} 
		catch (MalformedURLException e) {
			System.err.println("MalformedURLException happened while getting activity of bug " + bugId + ".\n");
			e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("In getBugActivity MalformedURLException happened while getting data about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting activity of bug " + bugId + ".\n");
			e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("In getBugActivity IOException happened while getting data about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
		}
		return activitiesList;
	}
	
	/*
	 * Funkcja zwracaj�ca u�ytkownik�w powi�zanych z b��dem 
	 * activitiesList - lista aktywno�ci powi�zanych z b��dem 
	 * activityHeuristic - heurystyka potrzebna przy przydzielaniu r�l programistom
	 * excludeWords - lista wykluczonych s��w 
	 */
	public ArrayList<Pair<User, String>> getDevelopersConnectedToBug(ArrayList<Activity> activitiesList, ActivityHeuristic activityHeuristic, ArrayList<String> excludeWords) {
		Boolean assigner_chosen, solver_chosen, reviewer_chosen;
		assigner_chosen = false;
		solver_chosen = false;
		reviewer_chosen = false;
		ArrayList<Pair<User, String>> developersList = new ArrayList<Pair<User, String>>();
		Collections.reverse(activitiesList);

		for (Activity activity : activitiesList) {
			// Wszytkie role przypisane
			if (assigner_chosen == true && solver_chosen == true && reviewer_chosen == true) 
				break;
			boolean cond = true;
			if (excludeWords != null) {
				for (String excludeWord: excludeWords) {
					if (activity.getWho().contains(excludeWord))
						cond = false;
				}
			}
			if (cond) {
				if (activity.getWhat().equalsIgnoreCase("assignee")) {
					if (!assigner_chosen) {
						developersList.add(new Pair<User, String>(new User(activity.getWho()), "assigner"));
						assigner_chosen = true;
					}
				} 
				if (activity.getWhat().equalsIgnoreCase("status")) {
					if (activity.getAdded().equalsIgnoreCase("assigned")) {
						if (!assigner_chosen) {
							developersList.add(new Pair<User, String>(new User(activity.getWho()), "assigner"));
							assigner_chosen = true;
						}
					} 
				}
				if (activity.getWhat().equalsIgnoreCase(activityHeuristic.getSolverHeuristic().getFirst())) {
					if (activity.getAdded().equalsIgnoreCase(activityHeuristic.getSolverHeuristic().getSecond())) {
						if (!solver_chosen) {
							developersList.add(new Pair<User, String>(new User(activity.getWho()), "solver"));
							solver_chosen = true;
						}
					} 
				}
				if (activity.getWhat().equalsIgnoreCase(activityHeuristic.getReviewerHeuristic().getFirst())) {
					if (activity.getAdded().equalsIgnoreCase(activityHeuristic.getReviewerHeuristic().getSecond())) {
						if (!reviewer_chosen) {
							developersList.add(new Pair<User, String>(new User(activity.getWho()), "reviewer"));
							reviewer_chosen = true;
						}
					} 
				}
			}
		}
		return developersList;
	}
	
	/*
	 * Funkcja zwracajaca dane b��du 
	 * bugId - id b��du 
	 * methodType - type metody
	 * filenameStopWords - plik z lista stop words
	 * activityHeuristic - heurystyka potrzebna przy przydzielaniu r�l programistom
	 * excludeWords - lista wykluczonych s��w
	 * fwErrors - FileWriter do zapisywania komunikat�w o b��dach
	 */
	public Bug getBug(int bugId, MethodType methodType, ActivityHeuristic activityHeuristic, ArrayList<String> excludeWords, String filenameStopWords, boolean includeAssigners, boolean includeSolvers, boolean includeReviewers, FileWriter fwErrors) {
		User assigner = null;
		User solver = null;
		User reviewer = null;
		String title = getBugTitle(bugId, methodType, fwErrors);
		String component = getBugComponent(bugId, methodType, fwErrors);
		String desc = getBugDescription(bugId, methodType, fwErrors);
		String tmp = title + " " + component + " " + desc;
		
		// Tworzenie listy z programistami powi�zanymi z b��dem
		ArrayList<Pair<User, String>> developerList = getDevelopersConnectedToBug(getBugActivity(bugId, methodType, fwErrors), activityHeuristic, excludeWords);
		for (Pair<User, String> d : developerList) {
			if (d.getSecond().equals("assigner") && includeAssigners) {
				assigner = d.getFirst();
			} 
			else if (d.getSecond().equals("solver") && includeSolvers) {
				solver = d.getFirst();
			} 
			else if (d.getSecond().equals("solver") && includeReviewers) {
				reviewer = d.getFirst();
			}
		}
		// Tworzenie listy term�w
		TokenStream tokenStream = null;
		ArrayList<String> termsList = new ArrayList<String>();
		try {
			Analyzer analyzer = new StopAnalyzer();
			tokenStream = analyzer.tokenStream("contents", new StringReader(tmp));
			String[] stopWordsList = {""}; 
			stopWordsList = loadListFromFile(filenameStopWords);
			CharArraySet stopWordsSet = new CharArraySet(
					Arrays.asList(stopWordsList),
					false);
			tokenStream = new StopFilter(tokenStream, stopWordsSet);
			// OffsetAttribute offsetAttribute =
			// tokenStream.addAttribute(OffsetAttribute.class);
			CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);

			tokenStream.reset();
			while (tokenStream.incrementToken()) {
				// int startOffset = offsetAttribute.startOffset();
				// int endOffset = offsetAttribute.endOffset();
				String term = charTermAttribute.toString();
				if (excludeWords == null) {
					termsList.add(term);
				}
				else {
					boolean cond = true;
					for (String excludeWord: excludeWords) {
						if (term.contains(excludeWord))
							cond = false;
					}
					if (cond) 
						termsList.add(term);
				}
			}
			analyzer.close();
			termsList.sort(new Comparator<String>() {
				public int compare(String p1, String p2) {
					return p1.compareTo(p2);
				}
			});
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting data about bug " + bugId + ".");
			e.printStackTrace();
			try {
				if (fwErrors != null)
					fwErrors.write("In getBug IOException happened while getting data about bug " + bugId + ".\n");
			}
			catch(IOException e2) {
				e2.printStackTrace();
			}
		}
		return new Bug(bugId, assigner, solver, reviewer, termsList);
	}
	
	/*
	 * Funkcja zwracaj�ca list� z zamkni�tymi b��dami 
	 * methodType - type metody
	 * filenameBugs - plik do zapisywania wynik�w po�rednich (lista b��d�w)
	 * filenameTime - plik do zapisywania wynik�w po�rednich (czas trwania poszczeg�lnuch zada�)
	 * filenameDesc - plik do zapisywania wynik�w po�rednich (lista opis�w)
	 * filenameError - plik do zapisywania wynik�w po�rednich (lista error�w i wyj�tk�w przy tworzeniu listy b��d�w)
	 * filenameStopWords - plik z lista stop words  
	 * minLimit - dolny limit na ilo�� s��w
	 * includeAssigners, includeSolvers, includeReviewers - kt�re role s� rozstrzygane
	 * activityHeuristic - heurystyka potrzebna przy przydzielaniu r�l programistom
	 * excludeWords - lista wykluczonych s��w
	 * dateFrom - data, od kt�rej b��dy brane s� po uwag�
	 * orderBy - po jakim polu posortowac b��dy
	 */
	public ArrayList<Bug> getSolvedBugs(MethodType methodType, String filenameBugs, String filenameTime,
			String filenameDesc, String filenameErrors, String filenameStopWords, int minLimit, 
			boolean includeAssigners, boolean includeSolvers, boolean includeReviewers, ActivityHeuristic activityHeuristic, ArrayList<String> excludeWords, String dateFrom, String orderBy) {
		ArrayList<String> termsList = new ArrayList<String>();
		ArrayList<String> rareTermsList = new ArrayList<String>();
		ArrayList<Bug> solvedBugsList = new ArrayList<Bug>();
		ArrayList<Bug> newSolvedBugsList = new ArrayList<Bug>();
		FileWriter fwBugs = null;
		FileWriter fwTime = null;
		FileWriter fwDesc = null;
		FileWriter fwErrors = null;
		try {
			allowWithoutCheckingCerts();

			if (filenameBugs != null && filenameTime != null && filenameDesc != null) {
				fwBugs = new FileWriter(filenameBugs);
				fwTime = new FileWriter(filenameTime);
				fwDesc = new FileWriter(filenameDesc);
			}
			if (filenameErrors != null) {
				fwErrors = new FileWriter(filenameErrors);
			}

			//String descUrl = url + "/buglist.cgi?bug_resolution=" + activityHeuristic.getStatusForClosed() + "&bug_status=" + activityHeuristic.getResolutionForClosed() + "&chfieldfrom=" + dateFrom + "&chfieldto=Now&columnlist=bug_id&limit=0&order=bug_id&query_based_on=&query_format=advanced";
			
			String descUrl = url + "/buglist.cgi?bug_status=" + activityHeuristic.getStatusForClosed() + "&chfieldfrom=" + dateFrom + "&chfieldto=Now&columnlist=" + orderBy + "&limit=10000&order=" + orderBy + "&query_format=advanced&resolution=" + activityHeuristic.getResolutionForClosed();
			//System.out.println(descUrl);
			URL realUrl = new URL(descUrl);

			BufferedReader in = new BufferedReader(new InputStreamReader(realUrl.openStream()));
			String inputLine;
			String result = "";
			while ((inputLine = in.readLine()) != null) {
				result = result + inputLine + "\n";
			}
			in.close();

			Document doc = Jsoup.parse(result); // Sparsowanie html
			if (doc == null)
				return solvedBugsList;
			Elements elems = doc.select("input[name=id]");
			if (elems == null)
				return solvedBugsList;
			
			int i = 0;
			if (filenameBugs != null && filenameTime != null && filenameDesc != null)
				fwTime.write("GENERATING LIST OF SOLVED BUGS: " + "\n");
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date dateStart = new Date();
			System.out.println("Number of bugs: " + elems.size());
			if (filenameTime != null && minLimit <= 0)
				fwTime.write(dateFormat.format(dateStart).toString() + "\n");
			for (Element elem : elems) {
				if (elem.val().matches("\\d+")) {
					System.out.println("Bug " + i + " - bug nr. " + Integer.parseInt(elem.val()));
					Bug bug = getBug(Integer.parseInt(elem.val()), methodType, activityHeuristic, excludeWords, filenameStopWords, includeAssigners, includeSolvers, includeReviewers, fwErrors);
					if (bug.getTerms().size() > 0 && ((bug.getAssigner() != null && includeAssigners)
							|| (bug.getSolver() != null && includeSolvers) 
							|| (bug.getReviewer() != null && includeReviewers))) {
						solvedBugsList.add(bug);
						if (filenameBugs != null && filenameTime != null && filenameDesc != null && minLimit <= 0) {
							fwBugs.write(bug.getId() + " "
									+ (bug.getAssigner() == null ? "NULL" : bug.getAssigner().toString()) + " "
									+ (bug.getSolver() == null ? "NULL" : bug.getSolver().toString()) + " "
									+ (bug.getReviewer() == null ? "NULL" : bug.getReviewer().toString()) + " ");
							bug.writeTermsToFile(fwBugs);
							bug.writeTermsToFile(fwDesc);
							if (minLimit > 0)
								termsList.addAll(bug.getTerms());
						}
					}
					else {
						if (fwErrors != null)
							fwErrors.write("In getSolvedBugs no terms or developers for bug " + bug.getId() + ".\n");
					}
				}
				i++;
			}
			if (minLimit > 0) {
				termsList.sort(new Comparator<String>() {
					public int compare(String p1, String p2) {
						return p1.compareTo(p2);
					}
				});
				String prevTerm = "";
				int counter = 0;
				for (String term: termsList) {
					if (prevTerm.equals(term)) {
						counter++;
					}
					else {
						if (counter < minLimit) 
							rareTermsList.add(prevTerm);
						prevTerm = term;
						counter = 1;
					}
				}
				if (counter < minLimit)
					rareTermsList.add(prevTerm);
				// Usuwa rzadkie termy 
				for (Bug bug: solvedBugsList) {
					bug.removeRareTerms(rareTermsList);
				}
				// Usuni�cie pustych b��d�w i zapisanie wynik�w do plik�w
				for (Bug bug: solvedBugsList) {
					if (bug.getTerms().size() > 0) {
						newSolvedBugsList.add(bug);
							if (filenameBugs != null && filenameTime != null && filenameDesc != null) {
								fwBugs.write(bug.getId() + " "
										+ (bug.getAssigner() == null ? "NULL" : bug.getAssigner().toString()) + " "
										+ (bug.getSolver() == null ? "NULL" : bug.getSolver().toString()) + " "
										+ (bug.getReviewer() == null ? "NULL" : bug.getReviewer().toString()) + " ");
							bug.writeTermsToFile(fwBugs);
							bug.writeTermsToFile(fwDesc);
						}
					}
					else {
						if (fwErrors != null)
							fwErrors.write("In getSolvedBugs no terms or developers for bug " + bug.getId() + ".\n");
					}
				}
				solvedBugsList = newSolvedBugsList;
			}
			Date dateEnd = new Date();
			if (filenameTime != null)
				fwTime.write(dateFormat.format(dateEnd).toString() + "\n");

			if (filenameBugs != null && filenameTime != null && filenameDesc != null) {
				fwBugs.close();
				fwTime.close();
				fwDesc.close();
			}
			if (filenameErrors != null) {
				fwErrors = new FileWriter(filenameErrors);
			}

		} 
		catch (MalformedURLException e) {
			System.err.println("MalformedURLException happened while getting list with solved bugs.");
			e.printStackTrace();
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting list with solved bugs.");
			e.printStackTrace();
		}
		return solvedBugsList;
	}

	/*
	 * Funkcja zwracaj�ca list� z zamkni�tymi b��dami z pliku
	 * filenameBugs - plik do zapisywania wynik�w po�rednich (lista b��d�w)
	 * minLimit - dolny limit na ilo�� s��w
	 * filenameBugsLimit - plik do zapisywania wynik�w po�rednich (lista b��d�w) kiedy limit > 0
	 * filenameTimeLimit - plik do zapisywania wynik�w po�rednich (czas trwania poszczeg�lnuch zada�) kiedy limit > 0
	 * filenameDescLimit - plik do zapisywania wynik�w po�rednich (lista opis�w) kiedy limit > 0
	 * filenameErrorLimit - plik do zapisywania wynik�w po�rednich (lista error�w i wyj�tk�w przy tworzeniu listy b��d�w) kiedy limit > 0
	 */
	public ArrayList<Bug> getSolvedBugsFromFile(String filenameBugs, int minLimit, String filenameBugsLimit, String filenameDescLimit, String filenameTimeLimit, String filenameErrorsLimit) {
		ArrayList<Bug> solvedBugsList = new ArrayList<Bug>();
		FileReader frBugs = null;

		try {

			frBugs = new FileReader(filenameBugs);
			BufferedReader br = new BufferedReader(frBugs);

			String line;
			String[] parts;
			while ((line = br.readLine()) != null) {
				int id;
				User assigner;
				User solver;
				User reviewer;
				ArrayList<String> terms = new ArrayList<String>();

				parts = line.split(" ");
				id = Integer.parseInt(parts[0]);
				if (parts[1].equals("NULL"))
					assigner = null;
				else
					assigner = new User(parts[1]);
				
				if (parts[2].equals("NULL"))
					solver = null;
				else
					solver = new User(parts[2]);
				
				if (parts[3].equals("NULL"))
					reviewer = null;
				else
					reviewer = new User(parts[3]);
				for (int i = 4; i < parts.length; i++)
					terms.add(parts[i]);
				solvedBugsList.add(new Bug(id, assigner, solver, reviewer, terms));
			}
			frBugs.close();
			ArrayList<String> termsList = new ArrayList<String>();
			ArrayList<String> rareTermsList = new ArrayList<String>();
			ArrayList<Bug> newSolvedBugsList = new ArrayList<Bug>();
			FileWriter fwBugs = null;
			FileWriter fwTime = null;
			FileWriter fwDesc = null;
			FileWriter fwErrors = null;
			if (minLimit > 0) {
				if (filenameBugsLimit != null && filenameTimeLimit != null && filenameDescLimit != null) {
					fwBugs = new FileWriter(filenameBugsLimit);
					fwTime = new FileWriter(filenameTimeLimit);
					fwDesc = new FileWriter(filenameDescLimit);
				}
				if (filenameErrorsLimit != null) {
					fwErrors = new FileWriter(filenameErrorsLimit);
				}
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date dateStart = new Date();
				if (filenameTimeLimit != null)
					fwTime.write(dateFormat.format(dateStart).toString() + "\n");
				termsList.sort(new Comparator<String>() {
					public int compare(String p1, String p2) {
						return p1.compareTo(p2);
					}
				});
				String prevTerm = "";
				int counter = 0;
				for (String term: termsList) {
					if (prevTerm.equals(term)) {
						counter++;
					}
					else {
						if (counter < minLimit) 
							rareTermsList.add(prevTerm);
						prevTerm = term;
						counter = 1;
					}
				}
				if (counter < minLimit)
					rareTermsList.add(prevTerm);
				// Usuwa rzadkie termy 
				int i = 0;
				for (Bug bug: solvedBugsList) {
					bug.removeRareTerms(rareTermsList);
					System.out.println("I " + i);
					i++;
				}
				// Usuni�cie pustych b��d�w i zapisanie wynik�w do plik�w
				i = 0;
				for (Bug bug: solvedBugsList) {
					System.out.println("J " + i);
					i++;
					if (bug.getTerms().size() > 0) {
						newSolvedBugsList.add(bug);
							if (filenameBugsLimit != null && filenameTimeLimit != null && filenameDescLimit != null) {
								fwBugs.write(bug.getId() + " "
										+ (bug.getAssigner() == null ? "NULL" : bug.getAssigner().toString()) + " "
										+ (bug.getSolver() == null ? "NULL" : bug.getSolver().toString()) + " "
										+ (bug.getReviewer() == null ? "NULL" : bug.getReviewer().toString()) + " ");
							bug.writeTermsToFile(fwBugs);
							bug.writeTermsToFile(fwDesc);
						}
					}
					else {
						if (fwErrors != null)
							fwErrors.write("In getSolvedBugs no terms or developers for bug " + bug.getId() + ".\n");
					}
				}
				solvedBugsList = newSolvedBugsList;
				Date dateEnd = new Date();
				if (filenameTimeLimit != null)
					fwTime.write(dateFormat.format(dateEnd).toString() + "\n");

				if (filenameBugsLimit != null && filenameTimeLimit != null && filenameDescLimit != null) {
					fwBugs.close();
					fwTime.close();
					fwDesc.close();
				}
				if (filenameErrorsLimit != null) {
					fwErrors = new FileWriter(filenameErrorsLimit);
				}
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened while generating bugs list from file.");
			e.printStackTrace();
		}
		return solvedBugsList;
	}
	
	/*
	 * Tworzy instancje potrzebne przy generowaniu topic modelu 
	 * filenameDesc - plik z termami z raport�w o b��dach
	 * filenameStopWords - plik z lista stop words   
	 */
	private InstanceList generateInstances(String filenameDesc, String filenameStopWords) {
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

		pipeList.add(new CharSequenceLowercase());
		pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
		// W filenameStopWords znajduj� si� domy�lne "stop words" w j�zyku angielskim
		pipeList.add(new TokenSequenceRemoveStopwords(new File(filenameStopWords), "UTF-8", false, false, false));
		pipeList.add(new TokenSequence2FeatureSequence());

		InstanceList instances = new InstanceList(new SerialPipes(pipeList));

		try {
			Reader fileReader = new InputStreamReader(new FileInputStream(new File(filenameDesc)), "UTF-8");
			instances.addThruPipe(
					new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"), 3, 2, 1));
		} 
		catch (IOException e) {
			System.err.println("IOException happened while generating instances for topic model.");
			e.printStackTrace();
		}
		return instances;
	}

	/*
	 * Funkcja tworz�ca topic model ze s��w w projekcie 
	 * filenameDesc - plik z termami z raport�w o b��dach 
	 * filenameStopWords - plik z lista stop words  
	 * numTopics - liczba kategorii
	 */
	public ParallelTopicModel generateTopicModel(String filenameDesc, String filenameStopWords, int numTopics) {
		ParallelTopicModel model = null;
		try {
			InstanceList instances = generateInstances(filenameDesc, filenameStopWords);

			// alpha_t = 0.01, beta_w = 0.01
			model = new ParallelTopicModel(numTopics, 1.0, 0.01);
			model.addInstances(instances);
			model.setNumThreads(2);

			// 10 tys iteracji
			model.setNumIterations(10000);
			model.estimate();
		} 
		catch (IOException e) {
			System.err.println("IOException happened while generating topics model.");
			e.printStackTrace();
		} 
		catch (UnsupportedOperationException e) {
			System.err.println("UnsupportedOperationException happened while generating topics model.");
			e.printStackTrace();
		}
		return model;
	}
	
	/*
	 * Funkcja zwracaj�ca i-t� kategori� w modelu
	 * model - topic model
	 * numTopic - liczba ketegori
	 * instances - instacje
	 */
	public Topic getTopicFromTopicsModel(ParallelTopicModel model, int numTopic, InstanceList instances) {
		ArrayList<Pair<String, Double>> topicWords = new ArrayList<Pair<String, Double>>();
		Alphabet dataAlphabet = instances.getDataAlphabet();
		ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();

		Iterator<IDSorter> iterator = topicSortedWords.get(numTopic).iterator();
		if (iterator == null)
			return null;
		while (iterator.hasNext()) {
			IDSorter idCountPair = iterator.next();
			topicWords.add(new Pair<String, Double>(dataAlphabet.lookupObject(idCountPair.getID()).toString(),
					idCountPair.getWeight()));
		}
		// Termy w kategorii posortowane leksykograficznie
		topicWords.sort(new Comparator<Pair<String, Double>>() {
			public int compare(Pair<String, Double> p1, Pair<String, Double> p2) {
				return p1.getFirst().compareTo(p2.getFirst());
			}
		});
		return new Topic(numTopic, topicWords);
	}
	
	/*
	 * Funkcja zwracaj�ca list� kategorii w modelu 
	 * numTopic - liczba ketegori
	 * filenameModel - plik, do ktorego zapisany zostanie model 
	 * filenameDesc - plik z termami z raport�w o b��dach
	 * filenameStopWords - plik z lista stop words   
	 */
	public ArrayList<Topic> getTopicsFromTopicsModel(int numTopics, String filenameModel, String filenameDesc, String filenameTime, String filenameStopWords) {
		ArrayList<Topic> topicsList = new ArrayList<Topic>();

		FileWriter fwModel = null;
		PrintWriter pwErrors = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		try {
			if (filenameTime != null) {
				pwErrors = new PrintWriter(new BufferedWriter(new FileWriter(filenameTime, true)));
				pwErrors.println("GENERATING LIST OF TOPICS: ");
				Date dateStart = new Date();
				pwErrors.println(dateFormat.format(dateStart).toString());
			}
		
			InstanceList instances = generateInstances(filenameDesc, filenameStopWords);
			ParallelTopicModel model = generateTopicModel(filenameDesc, filenameStopWords, numTopics);
			if (filenameModel != null)
				fwModel = new FileWriter(filenameModel);

			int i = 0;
			Topic topic;
			while (i < numTopics) {
				topic = getTopicFromTopicsModel(model, i, instances);
				topicsList.add(topic);
				if (filenameModel != null)
					topic.writeToFile(fwModel);
				i++;
			}

			if (filenameModel != null)
				fwModel.close();
			
			if (filenameTime != null) {
				Date dateEnd = new Date();
				pwErrors.println(dateFormat.format(dateEnd).toString());
				pwErrors.close();
			}
			
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting list with topics.");
			e.printStackTrace();
		}
		return topicsList;
	}

	/*
	 * Funkcja zwracaj�ca list� kategorii z pliku
	 * filenameModel - plik z modelem
	 */
	public ArrayList<Topic> getTopicsFromFile(String filenameModel) {
		ArrayList<Topic> topicsList = new ArrayList<Topic>();
		FileReader frModel = null;

		try {
			frModel = new FileReader(filenameModel);
			BufferedReader br = new BufferedReader(frModel);

			String line;
			String[] parts;
			while ((line = br.readLine()) != null) {
				int id;
				ArrayList<Pair<String, Double>> topicTerms = new ArrayList<Pair<String, Double>>();
				String term;
				Double weight;
				Topic topic;

				parts = line.split(" ");
				id = Integer.parseInt(parts[0]);
				for (int i = 1; i < parts.length; i++) {
					if (i < parts.length - 1) {
						term = parts[i];
						i++;
						weight = Double.parseDouble(parts[i]);
						topicTerms.add(new Pair<String, Double>(term, weight));
					}
				}
				topic = new Topic(id, topicTerms);
				topicsList.add(topic);
			}
			frModel.close();
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting list with topics.");
			e.printStackTrace();
		}
		return topicsList;
	}
	
	/*
	 * Funkcja tworzy zbi�r u�ytkownik�w w projekcie
	 * bugList - lista b��d�w
	 * filenameUsers - plik do zapisania listy u�ytkownik�w
	 * filenameTime - plik do zapisywania wynik�w po�rednich (czas trwania poszczeg�lnych zada�)
	 */
	public ArrayList<User> getUsers(ArrayList<Bug> bugsList, String filenameUsers, String filenameTime) {
		FileWriter fwUsers = null;
		Set<User> users = new TreeSet<User>();
		ArrayList<User> usersList = new ArrayList<User>();

		PrintWriter pwErrors = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			if (filenameTime != null) {
				pwErrors = new PrintWriter(new BufferedWriter(new FileWriter(filenameTime, true)));
				pwErrors.println("GENERATING LIST OF USERS: ");
				Date dateStart = new Date();
				pwErrors.println(dateFormat.format(dateStart).toString());
			}
		
			for (Bug bug : bugsList) {
				if (bug.getAssigner() != null)
					users.add(bug.getAssigner());
				if (bug.getSolver() != null)
					users.add(bug.getSolver());
				if (bug.getReviewer() != null)
					users.add(bug.getReviewer());
			}
			if (filenameUsers != null) {
				fwUsers = new FileWriter(filenameUsers);
				for (User user : users) {
					fwUsers.write(user.getLogin() + "\n");
				}
				fwUsers.close();
			}
			
			if (filenameTime != null) {
				Date dateEnd = new Date();
				pwErrors.println(dateFormat.format(dateEnd).toString());
				pwErrors.close();
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting list with users.");
			e.printStackTrace();
		}
		usersList.addAll(users);
		return usersList;
	}

	/*
	 * Funkcja tworzy zbi�r u�ytkownik�w w projekcie z pliku
	 * filenameUsers - plik z list� u�ytkownik�w
	 */
	public ArrayList<User> getUsersFromFile(String filenameUsers) {
		Set<User> users = new HashSet<User>();
		ArrayList<User> usersList = new ArrayList<User>();
		FileReader frUsers = null;

		try {
			frUsers = new FileReader(filenameUsers);
			BufferedReader br = new BufferedReader(frUsers);

			String line;
			while ((line = br.readLine()) != null)
				users.add(new User(line));
			frUsers.close();
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting list with users.");
			e.printStackTrace();
		}
		usersList.addAll(users);

		return usersList;
	}
	
	/*
	 * Funkcja tworzy liczniki akcji
	 * bugsList - lista b��d�w
	 * filenameCounters - plik do zapisywania licznika akcji
	 * filenameTime - plik do zapisywania wynik�w po�rednich (czas trwania poszczeg�lnych zada�) 
	 * includeAssigners, includeSolvers, includeReviewers - czy uwzgl�dnia� konkretne role
	 */
	public ArrayList<Integer> getActionsCounters(ArrayList<Bug> bugsList, boolean includeAssigners, boolean includeSolvers, boolean includeReviewers, 
			String filenameCounter, String filenameTime) {
		FileWriter fwCounter = null;
		ArrayList<Integer> actionsCounter = new ArrayList<Integer>();
		int assigners = 0, solvers = 0, reviewers = 0;

		PrintWriter pwErrors = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			if (filenameTime != null) {
				pwErrors = new PrintWriter(new BufferedWriter(new FileWriter(filenameTime, true)));
				pwErrors.println("GENERATING ACTIONS COUNTERS: ");
				Date dateStart = new Date();
				pwErrors.println(dateFormat.format(dateStart).toString());
			}
			for (Bug bug : bugsList) {
				if (bug.getAssigner() != null)
					assigners++;
				if (bug.getSolver() != null)
					solvers++;
				if (bug.getReviewer() != null)
					reviewers++;
			}

			if (filenameCounter != null) {
				if (includeAssigners == false)
					assigners = 0;
				if (includeSolvers == false)
					solvers = 0;
				if (includeReviewers == false)
					reviewers = 0;
				fwCounter = new FileWriter(filenameCounter);
				fwCounter.write(assigners + "\n");
				fwCounter.write(solvers + "\n");
				fwCounter.write(reviewers + "\n");
				fwCounter.close();
			}
			if (filenameTime != null) {
				Date dateEnd = new Date();
				pwErrors.println(dateFormat.format(dateEnd).toString());
				pwErrors.close();
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting counter of actions.");
			e.printStackTrace();
		}
		actionsCounter.add(assigners);
		actionsCounter.add(solvers);
		actionsCounter.add(reviewers);

		return actionsCounter;
	}

	/*
	 * Funkcja tworzy liczniki akcji z pliku
	 * filenameCounter - plik z licznikami akcji
	 */
	public ArrayList<Integer> getActionsCountersFile(String filenameCounter) {
		ArrayList<Integer> actionsCounter = new ArrayList<Integer>();
		FileReader frCounter = null;

		try {
			frCounter = new FileReader(filenameCounter);
			BufferedReader br = new BufferedReader(frCounter);

			String line;
			while ((line = br.readLine()) != null)
				actionsCounter.add(Integer.parseInt(line));
			frCounter.close();
		} 
		catch (IOException e) {
			System.err.println("IOException happened while getting counter of actions.");
			e.printStackTrace();
		}
		return actionsCounter;
	}

	/*
	 * Funkcja zwracaj�ca liczb� powi�za� mi�dzy b��dem, a kategori�
	 * bug - b��d
	 * topic - kategoria 
	 */
	private int getAssociationsNumberBugTopic(Bug bug, Topic topic) {
		boolean cond = true;
		int i = 0, j = 0;
		int termsNum = bug.getTerms().size();
		int wordsNum = topic.getTerms().size();

		int associations = 0;
		String term;
		String word;
		while (cond) {
			if ((i >= termsNum) || (j >= wordsNum))
				break;
			term = bug.getTerms().get(i); // term w opisie b��du
			word = topic.getTerms().get(j).getFirst(); // term w kategorii topic modelu
			if (term.equals(word)) {
				associations++;
				i++;
			} 
			else if (term.compareTo(word) < 0)
				i++;
			else
				j++;
		}
		return associations;
	}

	/*
	 * Funkcja generuj�ca macierz wi���c� raporty o b��dach z kategoriami
	 * bugsList - lista b��d�w
	 * topicsList - lista kategorii
	 * filenameBT - plik do zapisania macierzy
	 * filenameTime - plik do zapisywania wynik�w po�rednich (czas trwania poszczeg�lnych zada�)
	 */
	public BugsTopicsMatrix generateBugsTopicsMatrix(ArrayList<Bug> bugsList, ArrayList<Topic> topicsList,
			String filenameBT, String filenameTime) {
		
		int bugsNum = bugsList.size();
		int topicsNum = topicsList.size();
		BugsTopicsMatrix matrix = new BugsTopicsMatrix(bugsNum, topicsNum);
		FileWriter fwBT = null;
		PrintWriter out = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		try {
			if (filenameBT != null)
				fwBT = new FileWriter(filenameBT);
			
			if (filenameTime != null) {
				out = new PrintWriter(new BufferedWriter(new FileWriter(filenameTime, true)));
				out.println("GENERATING BUGS TOPICS MATRIX: ");
				Date dateStart = new Date();
				out.println(dateFormat.format(dateStart).toString());
			}

			int associations = 0;
			for (int i = 0; i < bugsNum; i++) {
				for (int j = 0; j < topicsNum; j++) {
					associations = getAssociationsNumberBugTopic(bugsList.get(i), topicsList.get(j));
					matrix.setFieldValue(i, j, associations);
				}
			}
			matrix.adjustMatrix();
			if (filenameBT != null) {
				for (int i = 0; i < bugsNum; i++) {
					for (int j = 0; j < topicsNum; j++) {
						fwBT.write(matrix.getFieldValue(i, j) + " ");
					}				
					fwBT.write("\n");
				}
			}
			if (filenameBT != null)
				fwBT.close();
			
			if (filenameTime != null) {
				Date dateEnd = new Date();
				out.println(dateFormat.format(dateEnd).toString());
				out.close();
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened while creating matrix with Bugs Topics.");
		}
		return matrix;
	}
	
	/*
	 * Funkcja zwracaj�ca liczb� powi�za� mi�dzy kategori� a u�ytkownikiem
	 * btmatrix - macierz wi���ca b��dy z kategoriami
	 * topicNum - liczba kategorii
	 * user - u�ytkownik
	 * bugsList - lista b��d�w
	 * includeAssigners, includeSolvers, includeReviewers - czy uwzgl�dnia� konkretne role
	 */
	private double getAssociationsNumberTopicUser(BugsTopicsMatrix btmatrix, int topicNum, User user, ArrayList<Bug> bugsList,
			boolean includeAssigners, boolean includeSolvers, boolean includeReviewers) {
		double assoctiations = 0;
		
		Bug bug;				
		for (int i = 0; i < bugsList.size(); i++) {
			bug = bugsList.get(i);
			if (includeAssigners && bug.getAssigner() != null && bug.getAssigner().getLogin().equals(user.getLogin()))
				assoctiations += btmatrix.getFieldValue(i, topicNum);
			if (includeSolvers && bug.getSolver() != null && bug.getSolver().getLogin().equals(user.getLogin()))
				assoctiations += btmatrix.getFieldValue(i, topicNum);
			if (includeReviewers && bug.getReviewer() != null && bug.getReviewer().getLogin().equals(user.getLogin()))
				assoctiations += btmatrix.getFieldValue(i, topicNum);
		}
		return assoctiations;
	}

	/*
	 * Funkcja generuj�ca macierz wi���c� u�ytkownik�w z kategoriami
	 * btmatrix - macierz wi���ca b��dy z kategoriami
	 * usersList - lista u�ytkownik�w
	 * bugsList - lista b��d�w
	 * includeAssigners, includeSolvers, includeReviewers - czy uwzgl�dnia� konkretne role
	 * filenameTU - plik do zapisywania macierzy
	 * filenameTime - plik do zapisywania wynik�w po�rednich (czas trwania poszczeg�lnych zada�)
	 */
	public TopicsUsersMatrix generateTopicsUsersMatrix(BugsTopicsMatrix btmatrix,
			ArrayList<User> usersList, ArrayList<Bug> bugsList,
			boolean includeAssigners, boolean includeSolvers, boolean includeReviewers,
			String filenameTU, String filenameTime) {
		int usersNum = usersList.size();
		TopicsUsersMatrix matrix = new TopicsUsersMatrix(btmatrix.getTopicsNum(), usersNum);
		FileWriter fwTU = null;
		PrintWriter out = null;
		double associations = 0;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		try {
			if (filenameTU != null)
				fwTU = new FileWriter(filenameTU);
			
			if (filenameTime != null) {
				out = new PrintWriter(new BufferedWriter(new FileWriter(filenameTime, true)));
				out.println("GENERATING TOPICS USERS MATRIX: ");
				Date dateStart = new Date();
				out.println(dateFormat.format(dateStart).toString());
			}

			for (int i = 0; i < btmatrix.getTopicsNum(); i++) {
				for (int j = 0; j < usersNum; j++) {
					associations = getAssociationsNumberTopicUser(btmatrix, i, usersList.get(j), bugsList,
							includeAssigners, includeSolvers, includeReviewers);
					matrix.setFieldValue(i, j, associations);
				}
			}
			//matrix.adjustMatrix();
			if (filenameTU != null) {
				for (int i = 0; i < matrix.getTopicsNum(); i++) {
					for (int j = 0; j < matrix.getUsersNum(); j++) {
						fwTU.write(matrix.getFieldValue(i, j) + " ");
					}				
					fwTU.write("\n");
				}
			}
			if (filenameTU != null)
				fwTU.close();
			
			if (filenameTime != null) {
				Date dateEnd = new Date();
				out.println(dateFormat.format(dateEnd).toString());
				out.close();
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened while creating matrix with Topics Users.");
		}
		return matrix;
	}

	/*
	 * Funkcja zwracaj�ca liczb� powi�za� mi�dzy kategori� a kategori� z uwzgl�dniem r�l
	 * user - u�ytkownik
	 * topicNr - numer kategorii
	 * bugsList - lista b��d�w
	 * matrix - macierz wi���ca b��dy z kategoriami
	 * assignersWeight, solversWeight, reviewersWeight - wagi poszczeg�lnych r�l
	 */
	public double getAssociationsNumberTopicUserScore(User user, int topicNr, ArrayList<Bug> bugsList, BugsTopicsMatrix matrix,
			double assignersWeight, double solversWeight, double reviewersWeight) {
		double assignersAssociations = 0.0;
		double solversAssociations = 0.0;
		double reviewersAssociations = 0.0;
		for (int i = 0; i < bugsList.size(); i++) {
			if (bugsList.get(i).getAssigner() != null && bugsList.get(i).getAssigner().getLogin().equals(user.getLogin())) {
				assignersAssociations += matrix.getFieldValue(i, topicNr);
			}
			if (bugsList.get(i).getSolver() != null && bugsList.get(i).getSolver().getLogin().equals(user.getLogin())) {
				solversAssociations += matrix.getFieldValue(i, topicNr);
			}
			if (bugsList.get(i).getReviewer() != null && bugsList.get(i).getReviewer().getLogin().equals(user.getLogin())) {
				reviewersAssociations += matrix.getFieldValue(i, topicNr);
			}
		}
		double sum = assignersAssociations + solversAssociations + reviewersAssociations;
		if (sum == 0)
			sum = 1;
		
		return (assignersAssociations/sum)*assignersWeight + (solversAssociations/sum)*solversWeight + (reviewersAssociations/sum)*reviewersWeight;
	}

	/*
	 * Funkcja generuj�ca macierz Score
	 * bugsList - lista b��d�w
	 * topicsList - lista kategorii
	 * usersList - lista u�ytkownik�w
	 * assigners, solvers, reviewers - liczniki poszczeg�lnych akcji
	 * filenameSc - plik do zapisywania macierzy
	 * filenameTime - plik do zapisywania wynik�w po�rednich (czas trwania poszczeg�lnych zada�)
	 */
	public ScoreMatrix generateTopicsUsersMatrixScore(
			ArrayList<Bug> bugsList, ArrayList<Topic> topicsList, ArrayList<User> usersList,
			int assigners, int solvers, int reviewers,
			String filenameSc, String filenameTime) {
		int bugsNum = bugsList.size();
		int topicsNum = topicsList.size();
		BugsTopicsMatrix matrix = new BugsTopicsMatrix(bugsNum, topicsNum);
		ScoreMatrix matrixSc = new ScoreMatrix(topicsNum, usersList.size());
		FileWriter fwSc = null;
		PrintWriter out = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		try {
			if (filenameSc != null)
				fwSc = new FileWriter(filenameSc);
			
			if (filenameTime != null) {
				out = new PrintWriter(new BufferedWriter(new FileWriter(filenameTime, true)));
				out.println("GENERATING BUGS SCORE MATRIX: ");
				Date dateStart = new Date();
				out.println(dateFormat.format(dateStart).toString());
			}

			int associations = 0;
			for (int i = 0; i < bugsNum; i++) {
				for (int j = 0; j < topicsNum; j++) {
					associations = getAssociationsNumberBugTopic(bugsList.get(i), topicsList.get(j));
					matrix.setFieldValue(i, j, associations);
				}
			}
			
			double associationsDb = 0.0;
			double sumWeight = assigners + solvers + reviewers;
			double assignersWeight = ((double) assigners*10)/sumWeight;
			double solversWeight = ((double) solvers*10)/sumWeight;
			double reviewersWeight = ((double) reviewers*10)/sumWeight;
			for (int i = 0; i < matrixSc.getUsersNum(); i++) {
				for (int j = 0; j < topicsNum; j++) {
					associationsDb = getAssociationsNumberTopicUserScore(usersList.get(i), j, bugsList, matrix,
							assignersWeight, solversWeight, reviewersWeight);
					matrixSc.setFieldValue(j, i, associationsDb);
				}
			}	
			
			if (filenameSc != null) {
				for (int i = 0; i < matrixSc.getTopicsNum(); i++) {
					for (int j = 0; j < matrixSc.getUsersNum(); j++) {
						fwSc.write(matrixSc.getFieldValue(i, j) + " ");
					}				
					fwSc.write("\n");
				}
			}
			if (filenameSc != null)
				fwSc.close();
			if (filenameTime != null) {
				Date dateEnd = new Date();
				out.println(dateFormat.format(dateEnd).toString());
				out.close();
			}
		} 
		catch (IOException e) {
			System.err.println("IOException happened while creating matrix with Bugs Topics.");
		}
		return matrixSc;
	}
	
	/*
	 * Funkcja znajduj�ca znacz�ce kategorie
	 * matrix - macierz wi���ca b��dy z kategoriami
	 * vector - wektor z macierzy dla nowego b��du
	 * filenameSig - nazwa pliku do zapisywania znacz�cych kategorii
	 */
	private ArrayList<Integer> getSignificiantTopicNumbers(BugsTopicsMatrix matrix, BugsTopicsMatrix vector, int significiantTopicNum,
			String filenameSig) {
		// Tablica z parami kategoria, liczba powi�za�
		ArrayList<Pair<Integer, Double>> topicsCountTmp = new ArrayList<Pair<Integer, Double>>();
		ArrayList<Pair<Integer, Double>> topicsCount = new ArrayList<Pair<Integer, Double>>();
		ArrayList<Pair<Integer, Double>> topicsCountVector = new ArrayList<Pair<Integer, Double>>();
		
		FileWriter fwSig = null;
		double associations;
		double sum = 0.0;
		for (int i = 0; i < matrix.getTopicsNum(); i++) {
			associations = 0.0;
			for (int j = 0; j < matrix.getBugsNum(); j++) {
				associations += matrix.getFieldValue(j, i);
			}
			sum += associations;
			topicsCountTmp.add(new Pair<Integer, Double>(i, associations));
		}
		for (int i = 0; i < matrix.getTopicsNum(); i++) {
			topicsCount.add(new Pair<Integer, Double>(i, topicsCountTmp.get(i).getSecond()/sum));
		}
		for (int i = 0; i < vector.getTopicsNum(); i++) {
			topicsCountVector.add(new Pair<Integer, Double>(i, vector.getFieldValue(0, i)));
		}	
		
		topicsCountVector.sort(new Comparator<Pair<Integer, Double>>() {
			public int compare(Pair<Integer, Double> p1, Pair<Integer, Double> p2) {
				return -p1.getSecond().compareTo(p2.getSecond());
			}
		});

		ArrayList<Integer> significiantTopicList = new ArrayList<Integer>();
		try {
			if (filenameSig != null)
				fwSig = new FileWriter(filenameSig);
			int i = 0;
			int j = 0;
			while (j < significiantTopicNum) {
				if (i >= topicsCount.size())
					break;
				//if (topicsCountVector.get(i).getSecond() >= topicsCount.get(topicsCountVector.get(i).getFirst()).getFirst()/bugsNum) {
				if (topicsCountVector.get(i).getSecond() >= topicsCount.get(i).getSecond()) {
					significiantTopicList.add(topicsCountVector.get(i).getFirst());
					if (filenameSig != null)
						fwSig.write(topicsCount.get(i).getFirst() + "\n");
					j++;
				}
				i++;
			}
			if (significiantTopicList.size() == 0) {
				i = 0;
				while (i < 10) {
					if (i >= significiantTopicList.size())
					significiantTopicList.add(topicsCountVector.get(i).getFirst());
					if (filenameSig != null)
						fwSig.write(topicsCountVector.get(i).getFirst() + "\n");
					i++;
				}
			}
			if (filenameSig != null)
				fwSig.close();
		} 
		catch (IOException e) {
			System.err.println("IOException happened while creating matrix with Bugs Topics.");
		}
		return significiantTopicList;
	}
	
	/*
	 * Funkcja tworz�ca list� u�ytkownik�w do rozwi�zania b��du
	 * filenameStopWords - plik z lista stop words
	 */
	public ArrayList<User> generateUsersListForBug(int bugId, MethodType methodType, String filenameStopWords, ActivityHeuristic activityHeuristic, ArrayList<String> excludeWords, 
			String filenameSig, String filename,
			BugsTopicsMatrix matrixBT, TopicsUsersMatrix matrixTU, ScoreMatrix score, ArrayList<User> usersList, ArrayList<Topic> topicsList, boolean includeAssigners, boolean includeSolvers, boolean includeReviewers, int sigNum, int listSize) {
		ArrayList<Pair<Integer, Double>> usersCount = new ArrayList<Pair<Integer, Double>>();
		Bug bug = getBug(bugId, methodType, activityHeuristic, excludeWords, filenameStopWords, includeAssigners, includeSolvers, includeReviewers, null);
		ArrayList<Bug> bugsList = new ArrayList<Bug>();
		bugsList.add(bug);

		// Nowy wiersz w macierzy dla nowego b��du
		BugsTopicsMatrix bugTopicsVector = generateBugsTopicsMatrix(bugsList, topicsList, null, null);
		ArrayList<Integer> significiantTopicsNum = getSignificiantTopicNumbers(matrixBT, bugTopicsVector, sigNum, filenameSig);

		double associations;
		for (int j = 0; j < matrixTU.getUsersNum(); j++) {
			associations = 0.0;
			for (int i = 0; i < matrixTU.getTopicsNum(); i++) {
				if (significiantTopicsNum.contains(i))
					associations += matrixTU.getFieldValue(i, j) * bugTopicsVector.getFieldValue(0, i) * score.getFieldValue(i, j);
			}
			usersCount.add(new Pair<Integer, Double>(j, associations));
		}
		usersCount.sort(new Comparator<Pair<Integer, Double>>() {
			public int compare(Pair<Integer, Double> p1, Pair<Integer, Double> p2) {
				return -p1.getSecond().compareTo(p2.getSecond());
			}
		});

		ArrayList<User> usersForBug = new ArrayList<User>();
		FileWriter f1 = null;
		try {
			if (filename != null)
				f1 = new FileWriter(filename);
			for (int i = 0; i < listSize; i++) {
				if (i >= usersCount.size())
					break;
				usersForBug.add(usersList.get(usersCount.get(i).getFirst()));
				f1.write(usersList.get(usersCount.get(i).getFirst()).getLogin() + "\n");
			}
			if (filename != null)
				f1.close();
		} 
		catch (IOException e) {
			System.err.println("IOException happened while creating list of users for bug.");
		}
		return usersForBug;
	}
	
}
