import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/*
 * Klasa odpowiadaj�ca kategorii w modelu 
 */
public class Topic {

	/*
	 * Id kategorii
	 */
	private final int id;
	/*
	 * S�owa z danej kategorii (z wagami)
	 */
	private final ArrayList<Pair<String, Double>> terms;
	
	Topic(int id, ArrayList<Pair<String, Double>> terms) {
		this.id = id;
		this.terms = terms;
	}
	
	int getId() {
		return id;
	}
	
	ArrayList<Pair<String, Double>> getTerms() {
		return terms;
	}
	
	/*
	 * Funkcja zwracaj�ca kategori� w postaci: ID S�OWO1 WAGA1 S�OWO2 WAGA2...
	 */
	@Override
    public String toString() {
    	String str = "";
    	str = str + id + " ";
    	for (Pair<String, Double> term:  terms) {
    		str = str + term.getFirst() + " ";
    		str = str + term.getSecond() + " ";
    	}
    	return str;
    }
	
	/*
	 * Funkcja wypisuj�ca kategori� w postaci: ID S�OWO1 WAGA1 S�OWO2 WAGA2... do pliku
	 */
    public void writeToFile(FileWriter f1) throws IOException {
    	f1.write(id + " ");
    	for (Pair<String, Double> term:  terms) {
    		f1.write(term.getFirst() + " " + term.getSecond() + " ");
    	}
    	f1.write("\n");
    }
	
}