import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/*
 * Klasa odpowiadaj�ca raportowi o b��dzie w repozytorium
 */
public class Bug {

	/*
	 * Id b��du
	 */
	private final int id;
	/*
	 * U�ytkownik, kt�ry przypisa� b��d do programisty
	 */
	private final User assigner;
	/*
	 * U�ytkownik, kt�ry naprawi� b��d
	 */
	private final User solver;
	/*
	 * U�ytkownik, kt�ry sprawdzi� czy b��d zosta� poprawnie naprawiony
	 */
	private final User reviewer;
	/*
	 * S�owa znacz�ce (opisu nie trzymamy)
	 */
	private ArrayList<String> terms;
    
    public Bug(int id, User assigner, User solver, User reviewer, ArrayList<String> terms) {
    	this.id = id;
    	this.solver = solver;
    	this.assigner = assigner;
    	this.reviewer = reviewer;
    	this.terms = terms;
    }
    
    public int getId() {
    	return id;
    }
    
    public User getSolver() {
    	return solver;
    }
    
    public User getAssigner() {
    	return assigner;
    }
    
    public User getReviewer() {
    	return reviewer;
    }
    
    public void setTerms(ArrayList<String> terms) {
    	this.terms = terms;
    }
    
    public ArrayList<String> getTerms() {
    	return terms;
    }
    
    /*
     * Funkcja zwracaj�ca termy powiazane z danym b��dem w postaci: TERM1 TERM2 TERM3...
     */
    public String getTermsToString() {
    	String str = "";
    	int cond = 0;
    	for (String term: terms) {
    		if (cond != 0)
    			str += " ";
    		else 
    			cond = 1;
    		str += term;
    	}
    	return str;
    }
    
    /*
     * Funkcja wypisuj�ca termy powiazane z danym b��dem w postaci: TERM1 TERM2 TERM3... do pliku
     */
    public void writeTermsToFile(FileWriter fw) throws IOException {
    	int cond = 0;
    	for (String term: terms) {
    		if (cond != 0)
    			fw.write(" ");
    		else 
    			cond = 1;
    		fw.write(term);
    	}
    	fw.write("\n");
    }   
    
    /*
     * Usuwa rzadkie termy
     */
    public void removeRareTerms(ArrayList<String> rareTermsList) {
    	ArrayList<String> bugTermsList = new ArrayList<String>();
    	bugTermsList = getTerms();
    	bugTermsList.removeAll(rareTermsList);
    	setTerms(bugTermsList);
    }
	
}
