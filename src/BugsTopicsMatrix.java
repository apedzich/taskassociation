/*
 * Klasa odpowiadaj�ca macierzy wi���cej kategorie z b��dami
 */
public class BugsTopicsMatrix {

	/*
	 * Liczba b��d�w
	 */
	final private int bugsNum;
	/*
	 * Liczba kategorii
	 */
	final private int topicsNum;
	/*
	 * Macierz
	 */
	private double[][] matrix;
		
	BugsTopicsMatrix(int bugsNum, int topicsNum) {
		this.bugsNum = bugsNum;
		this.topicsNum = topicsNum;
		this.matrix = new double[bugsNum][topicsNum];
	}
		
	public void adjustMatrix() {
		for (int i = 0; i < bugsNum; i++) {
			double rowCounter = 0.0;
			for (int j = 0; j < topicsNum; j++) {
				rowCounter = rowCounter + matrix[i][j];
			}
			if (rowCounter != 0) {
				for (int j = 0; j < topicsNum; j++) {
					matrix[i][j] = matrix[i][j]/rowCounter;
				}
			}
		}
	}
		
	public int getBugsNum() {
		return bugsNum;
	}
		
	public int getTopicsNum() {
		return topicsNum;
	}
		
	public double getFieldValue(int bugNr, int topicNr) {
		return matrix[bugNr][topicNr];
	}
		
	public void setFieldValue(int bugNr, int topicNr, double value) {
		matrix[bugNr][topicNr] = value;
	}
		
	public void increaseFieldValue(int bugNr, int topicNr, double inc) {
		matrix[bugNr][topicNr] += inc;
	}
}