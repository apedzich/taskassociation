/*
 * Klasa odpowiadająca macierzy wiążącej kategorie z programistami
 */
public class TopicsUsersMatrix {

	/*
	 * Liczba kategorii
	 */
	final private int topicsNum;
	/*
	 * Liczba programistów
	 */
	final private int usersNum;
	/*
	 * Macierz
	 */
	private double[][] matrix;
			
	TopicsUsersMatrix(int topicsNum, int usersNum) {
		this.topicsNum = topicsNum;
		this.usersNum = usersNum;
		this.matrix = new double[topicsNum][usersNum];
	}
	
	public void adjustMatrix() {
		for (int i = 0; i < usersNum; i++) {
			double colCounter = 0.0;
			for (int j = 0; j < topicsNum; j++) {
				colCounter = colCounter + matrix[j][i];
			}
			if (colCounter != 0) {
				for (int j = 0; j < topicsNum; j++) {
					matrix[j][i] = matrix[j][i]/colCounter;
				}
			}
		}
	}
	
	public int getTopicsNum() {
		return topicsNum;
	}
			
	public int getUsersNum() {
		return usersNum;
	}

	public double getFieldValue(int topicNr, int userNr) {
		return matrix[topicNr][userNr];
	}
			
	public void setFieldValue(int topicNr, int userNr, double value) {
		matrix[topicNr][userNr] = value;
	}
			
	public void increaseFieldValue(int topicNr, int userNr, double inc) {
		matrix[topicNr][userNr] += inc;
	}

}