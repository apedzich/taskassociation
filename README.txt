Parametry:
projectName - string; nazwa projektu; pliki po�rednie b�d� mia�y nast�puj�ce nazwy:
		projectNameBugs.txt - plik z list� b��d�w
   		projectNameDesc.txt - plik ze s�owami w opisach b��d�w
		projectNameCounters.txt - plik z licznikami akcji
		projectNameTopics.txt - plik z kategoriami
    		projectNameErrors.txt - plik z b��dami 	
url - string; adres repozytorium Bugzilli projektu open source (lista b��d�w musi by� og�lnodost�pna)
excludeWordsFile - string; plik zawieraj�cy s�owa, kt�re zostan� wykluczone w analizie (heurystyka 2) lub null; kolejne s�owa znajduj� si� w kolejnych liniach
activityHeuristicFile - string; plik zawieraj�cy heurystyk�, na podstawie kt�rej mo�na przydzieli� programistom role; 
	wygl�d pliku:
		w pierwszej linii: warto�� pola Status dla zamkni�tych b��d�w
		w drugiej linii: warto�� pola Resolution dla zamkni�tych b��d�w
		w trzeciej linii: nazwa pola
		w czwartek linii: warto�� pola z linii trzeciej; programista, kt�ry zmieni� warto�� pola pe�ni� rol� Naprawiaj�cego problem
		w pi�tej linii: nazwa pola
		w sz�stej linii: warto�� pola z linii pi�tej; programista, kt�ry zmieni� warto�� pola pe�ni� rol� Weryfikuj�cego rozwi�zanie;
	przyk�adowa zawarto�� pliku (b��dy zamkni�te maj� warto�� pola Status ustawion� na RESOLVED i warto�� pola Resolution ustawion� na FIXED,
		programista, kt�ry zmieni� warto�� pola Status na RESOLVED, pe�ni� rol� Naprawiaj�cego problem,
		programista, kt�ry zmieni� warto�� pola Resolution na FIXED, pe�ni� rol� Weryfikuj�cego rozwi�zanie):
		RESOLVED
		FIXED
		status
		RESOLVED
		resolution
		FIXED
minLimit - int; dolny limit na ilo�� wyst�pie� s��w, aby zosta�y uwzgl�nione w analize (heurystyka 2); minLimit < 0 oznacza, �e heurystyka jest wy��czona
includeAssigners - boolean; czy uwzgl�dnia� rol� Przypisuj�cego programist� 
includeSolvers - boolean; czy uwzgl�dnia� rol� Naprawiaj�cego problem
includeReviewers - boolean; czy uwzgl�dnia� rol� Weryfikuj�cego rozwi�zanie
dateFrom - string; format YYYY-MM-DD; data, od kt�rej b��dy maj� by� uwzgl�dniane
orderBy - string; warto�� pola, po kt�rej uporz�dkowane maj� by� b��dy
testSetNum - int; liczba b��d�w w zestawie testowym
topicsNum - int; liczba kategorii b��d�w
sigNum - int; liczba istotnych kategorii
listSize - int; d�ugo�� listy programist�w
stopWordsFile - sting; plik zawieraj�cy list� Stop words; kolejne s�owa znajduj� si� w kolejnych liniach

Wyniki:

pliki z listami wygenerowanymi dla b��d�w: projectName_x.txt, gdzie x to id b��du
na standardowe wyj�cie: "Mean of successes in 10 iterations x", gdzie x to �rednia z liczby sukces�w uzyskanych w 10 iteracjach

Przyk�adowe wykorzystanie:
sudo https://bugzilla.sudo.ws excludeWords.txt activityHeuristic.txt -1 true true true 1990-01-01 relevance+desc 30 100 20 5 en.txt
pliki z wynikiami po�rednimi: sudoBugs.txt, sudoDesc.txt, sudoCounters.txt, sudoTopics.txt, sudoErrors.txt  